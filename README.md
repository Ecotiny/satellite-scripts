# Satellite Scripts
Scripts for recording and demodulation/decoding of satellite signals with the [rtl-sdr](https://www.rtl-sdr.com)    
**Currently only Meteor-M 2 and NOAA**

# Dependencies
+ rtl_sdr (preferably from [github](https://github.com/osmocom/rtl-sdr))
  + cmake (in apt)
    + ```-DINSTALL_UDEV_RULES=ON```
  + libusb-1.0-0-dev (in apt)
+ [mlrpt](http://www.5b4az.org) (Weather Imaging -> Meteor-M LRPT Reciever -> mlrpt)
  + autoconf (in apt)
  + rtl_sdr must be installed first
+ at (`sudo apt install at`)
+ **Python3 Packages**
  + pyorbital (`sudo apt install python3-pyorbital`)
  + numpy (`sudo apt install python3-numpy`)
  + dateutil (`sudo apt install python3-dateutil`)

# Configuration
+ All files assume this repo was cloned into the home directory (`/home/pi/satellite-scripts`)
+ The scheduler.py file requires configuration of the user's home directory (default `/home/pi`)

## TODO
+ Tune `rtl_fm` settings for NOAA reception (found in `schedule_noaa.sh`)
+ Figure out why `mlrpt` wasn't working