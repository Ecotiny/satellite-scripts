#!/bin/bash

FREQ=$1
DATE=$(date +%Y%m%d-%H%M)

mkdir /tmp/NOAA
SECONDS=0

#timeout $2 rtl_fm -f ${FREQ} -s 60k -g 45 -p -35 -E wav -E deemp -F 9 /tmp/NOAA/${DATE}.wav

timeout $2 rtl_fm -f ${FREQ} -s 44100 -g 20 -F 9 -A fast -E dc -E deemp /tmp/NOAA/${DATE}.wav
echo "Seconds spent recording: ${SECONDS}"

rsync -avh /tmp/NOAA/ home.4i.nz:noaa_raw/ --delete

