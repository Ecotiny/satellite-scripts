from pyorbital.orbital import Orbital
import datetime
import urllib3
import subprocess
from dateutil import tz

lat=-45.856788075
lon=170.529
alt=95

user_dir = "/home/pi"

print("Location: {}N, {}E, {}m elevation".format(lat,lon,alt))

http = urllib3.PoolManager()

TLE_url = "http://www.celestrak.com/NORAD/elements/weather.txt"


subprocess.run(["wget", TLE_url, "-O", "tle.txt"]) 

sat_dict = {"NOAA 15" : "137.62M", "NOAA 18" : "137.9125M", "NOAA 19" : "137.1M", "METEOR-M 2" : "137.9M"}

satellites = []

for name in sat_dict.keys():
    sat = Orbital(name, tle_file=user_dir + "/satellite-scripts/tle.txt")
    satellites.append(sat)
    
print("===RECEIVED_TLE===")

for sat in satellites:
    # HAVEN'T CONFIGURED METEOR YET
    
    print("==={}===".format(sat.satellite_name))
    
    passes = sat.get_next_passes(datetime.datetime.utcnow(), 24, lon, lat, alt, tol=0.001, horizon=0)
    # each pass rise_time, fall_time, max_el_time
    for pass_details in passes:
        rise_time, set_time, max_time = pass_details
        # convert from utc to local (tell dt that it is in utc first)
        rise_time = rise_time.replace(tzinfo=tz.tzutc()).astimezone(tz.tzlocal())
        set_time =   set_time.replace(tzinfo=tz.tzutc()).astimezone(tz.tzlocal())
        max_time =   max_time.replace(tzinfo=tz.tzutc()).astimezone(tz.tzlocal())
        
        duration = set_time - rise_time
        
        formatString = "%H:%M %b %d"
        formatted_rise = rise_time.strftime(formatString)
        # format with lowercase AM/PM and no padded 0 date
        new_formatted = formatted_rise[0:2] + formatted_rise[2:4].lower() + formatted_rise[4:]
        if new_formatted[0] == '0':
            new_formatted = new_formatted[1:]
        # 10am Jul 31
        frequency = sat_dict[sat.satellite_name]
        if sat.satellite_name == "METEOR-M 2":
            cmd_array = [user_dir + "/satellite-scripts/schedule_meteor.sh", new_formatted]
        else:
            cmd_array = [user_dir + "/satellite-scripts/schedule_noaa.sh", frequency, new_formatted, str(duration.seconds)]
        subprocess.run(cmd_array)
        print(cmd_array)
